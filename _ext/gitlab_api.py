"""
Extension to insert data pulled from GitLab API into documents
"""

import datetime
import os
import re
import shelve
from pathlib import Path

import gitlab
import pypandoc
from docutils import nodes
from docutils.parsers.rst import Directive, directives
from docutils.statemachine import ViewList
from jinja2 import Environment
from sphinx.util.nodes import nested_parse_with_titles


def md2rst(text):
    """Jinja2 filter to convert Markdown to ReST

    :param text: Markdown text to convert from
    :type text: str
    :return: Text converted to ReST
    :rtype: str
    """
    return pypandoc.convert_text(text, format='markdown', to='rst')


def create_node(state, content):
    """Parse content and create node

    :param state: docutils state machine
    :type state: class: docutils.statemachine.State
    :param content: Content to parse
    :type content: str
    :return: Node
    :rtype: class:`docutils.nodes.Node`
    """

    # Create emtpy node
    node = GitlabNode()

    # Create viewlist of content
    rst = ViewList(content.splitlines(), source='')

    # Parse content and return node
    nested_parse_with_titles(state, rst, node)

    return node


def create_gitlab_client():
    """Create a GitLab client object

    :return: GitLab client object
    :rtype: class:`gitlab.Gitlab`
    """
    options = {}
    env_map = {
        'GITLAB_URL': 'server_url',
        'GITLAB_SSL_VERIFY': 'ssl_verify',
        'GITLAB_TIMEOUT': 'timeout',
        'GITLAB_API_VERSION': 'api_version',
        'GITLAB_PER_PAGE': 'per_page',
        'GITLAB_PAGINATION': 'pagination',
        'GITLAB_ORDER_BY': 'order_by',
        'GITLAB_USER_AGENT': 'user_agent',
        'GITLAB_PRIVATE_TOKEN': 'private_token',
        'GITLAB_OAUTH_TOKEN': 'oauth_token',
    }

    # Create options dictionary from environment variables
    for env_name, option in env_map.items():
        env_val = os.getenv(env_name)
        if env_val:
            options[option] = env_val

    # Create client
    gl_client = gitlab.Gitlab.merge_config(options)

    return gl_client


class GitlabNode(nodes.Element):
    """Empty docutils node
    """


class GitlabCache():
    """Cache for GitLab data

    :param cachefile_path: Path to cache file
    :type cachefile_path: class:`pathlib.Path`
    """
    VALIDITY_MINS = 5

    def __init__(self, cachefile_path):
        """Class constructor"""
        self.cachefile_path = cachefile_path

        # Create parent directory path
        Path(self.cachefile_path.parent).mkdir(parents=True, exist_ok=True)

    def get(self, key):
        """Get cache entry

        :param key: Key of entry to get
        :type key: str
        :return: Value of cache entry
        :rtype: str or None
        """
        try:
            with shelve.open(self.cachefile_path) as cache:
                try:
                    # Check if entry is invalid
                    if datetime.datetime.now(
                            tz=datetime.UTC
                    ) - cache[key]['timestamp'] > datetime.timedelta(
                            minutes=GitlabCache.VALIDITY_MINS):
                        return None
                except KeyError:
                    return None
                # Return content of entry
                return cache[key]['content']
        except OSError:
            return None

    def set(self, key, content):
        """Set or update cache entry

        :param key: Key of entry to set or update
        :type key: str
        :param content: Value to set or update the entry with
        :type content: str
        """
        with shelve.open(self.cachefile_path) as cache:
            cache[key] = {
                'timestamp': datetime.datetime.now(tz=datetime.UTC),
                'content': content,
            }


class GitlabUserDirective(Directive):
    """GitLab user name and profile URL"""
    option_spec = {
        'username': directives.unchanged_required,
    }

    def run(self):

        gitlabcache = GitlabCache(
            self.state.document.settings.env.srcdir.joinpath(
                '.gitlab_api/cache'))

        # Get user from cache
        user = gitlabcache.get(f'gitlabuser/{self.options["username"]}')

        if user is None:

            # Create GitLab client
            gl_client = create_gitlab_client()

            # Get user
            try:
                user = gl_client.users.list(
                    username=self.options['username'])[0]
            except gitlab.exceptions.GitlabError:
                node = create_node(self.state, self.options["username"])
                return node.children

            # Update cache
            gitlabcache.set(f'gitlabuser/{self.options["username"]}', user)

            # Create node
        node = create_node(self.state, f'`{user.name} <{user.web_url}>`_')

        return node.children


class GitlabIssuesDirective(Directive):
    """Gitlab project issues rendered using a Jinja2 template"""
    has_content = True
    option_spec = {
        'project': directives.path,
        'labels': directives.unchanged_required,
        'sections_regex': directives.unchanged_required,
    }

    def _get_issues(self):
        """Get issues from GitLab

        :return: GitLab project issue list iterator
        :rtype: iterator
        """

        gitlabcache = GitlabCache(
            self.state.document.settings.env.srcdir.joinpath(
                '.gitlab_api/cache'))

        # Get issues from cache
        issues = gitlabcache.get(
            f'gitlabissues/{self.options["project"]}/{self.options["labels"]}')

        if issues is None:

            # Create GitLab client
            gl_client = create_gitlab_client()

            # Get project
            project = gl_client.projects.get(self.options['project'],
                                             lazy=True)

            # Get issues with labels
            try:
                issues = project.issues.list(labels=[
                    x.strip() for x in self.options['labels'].split(',')
                ],
                                             iterator=True,
                                             state='opened',
                                             order_by='created_at',
                                             sort='asc')
            except gitlab.exceptions.GitlabError:
                return []

            # Update cache
            gitlabcache.set(
                f'gitlabissues/{self.options["project"]}/{self.options["labels"]}',
                issues)

        return issues

    def _parse_issues(self, issues):
        """Parse Gitlab issues

        :param issues: GitLab project issue list iterator
        :type issues: iterator
        :return: Parsed list of issues
        :rtype: list
        """

        # Create parsed issues list
        items = []

        for issue in issues:

            # Parse issue
            desc = re.findall(self.options['sections_regex'],
                              issue.description, re.MULTILINE | re.DOTALL)

            # Create issue item
            item = {
                'id': issue.iid,
                'title': issue.title,
                'labels': issue.labels,
                'confidential': issue.confidential,
                'content': dict(desc),
            }

            # Append to items list
            items.append(item)

        return items

    def _render_template(self, content, issues):
        """Render Jinja2 template

        :param content: Jinja2 template
        :type content: str
        :param issues: Parsed list of issues
        :type issues: list
        :return: Rendered document
        :rtype: str
        """

        # Create Jinja2 environment
        j2_env = Environment()
        j2_env.filters['md2rst'] = md2rst

        # Load Jinja2 template
        template = j2_env.from_string(content)

        return template.render(issues=issues)

    def run(self):

        # Get GitLab issues
        gitlab_issues = self._get_issues()

        # Parse issues
        issues = self._parse_issues(gitlab_issues)

        # Create document
        document = self._render_template('\n'.join(self.content), issues)

        # Create node
        node = create_node(self.state, document)
        return node.children


def setup(app):
    app.add_directive('gitlabissues', GitlabIssuesDirective)
    app.add_directive('gitlabuser', GitlabUserDirective)
